# syntax = docker/dockerfile:1.4
ARG FROM_IMG_REGISTRY=registry.gitlab.com
ARG FROM_IMG_REPO=qnib-pub-containers/qnib/uplain
ARG FROM_IMG_NAME=openjdk-jre-headless
ARG FROM_IMG_TAG="2023-05-31"

FROM ${FROM_IMG_REGISTRY}/${FROM_IMG_REPO}/${FROM_IMG_NAME}:${FROM_IMG_TAG}

ARG GOCD_URL=https://download.gocd.io/binaries
ARG GOCD_VER=22.3.0
ARG GOCD_SUBVER=15301

ENV GO_SERVER_URL=https://tasks.server:8154/go \
    GOCD_LOCAL_DOCKERENGINE=false \
    GOCD_CLEAN_IMAGES=false \
    DOCKER_TAG_REV=true \
    GOCD_AGENT_AUTOENABLE_KEY=qnibFTW \
    GOCD_AGENT_AUTOENABLE_ENV=latest,upstream,docker,deploy \
    GOCD_AGENT_AUTOENABLE_RESOURCES=ubuntu \
    DOCKER_REPO_DEFAULT=qnib \
    GOPATH=/usr/local/ \
    DOCKER_CONSUL_DNS=false \
    LANG=en_US.utf8 \
    HOME_DIR=/home \
    ENTRY_USER=gocd 

LABEL gocd.version=${GOCD_VER}-${GOCD_SUBVER}
# allow mounting ssh keys, dotfiles, and the go server config and data
VOLUME /godata
RUN adduser --home /opt/go-agent/ --shell /sbin/nologin --uid=5000 --shell /usr/sbin/nologin gocd
RUN <<eot bash
 apt update
 apt install -y python3-pip git moreutils libarchive-tools iproute2 skopeo
 rm -rf /var/lib/apt/lists/*
eot
RUN <<eot bash
 VERSION=$(curl -s "https://api.github.com/repos/ahmet2mir/wildq/releases/latest" | grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/')
 curl -sL https://github.com/ahmet2mir/wildq/releases/download/v${VERSION}/wildq_${VERSION}-1_amd64.deb -o wildq_${VERSION}-1_amd64.deb
 dpkg -i wildq_${VERSION}-1_amd64.deb
 rm -f wildq_${VERSION}-1_amd64.deb
eot
## Allow for reusable file-system layers
RUN mkdir -p /opt/go-agent/ \
 && echo "Download '${GOCD_URL}/${GOCD_VER}-${GOCD_SUBVER}/generic/go-agent-${GOCD_VER}-${GOCD_SUBVER}.zip'" \
 && wget -qO - ${GOCD_URL}/${GOCD_VER}-${GOCD_SUBVER}/generic/go-agent-${GOCD_VER}-${GOCD_SUBVER}.zip |bsdtar xfz - -C /opt/go-agent/ --strip-components=1 \
 && chmod +x /opt/go-agent/bin/go-agent
RUN wget -qO - $(/usr/local/bin/go-github rLatestUrl --ghorg qnib --ghrepo service-scripts --regex ".*.tar" --limit 1) |tar xf - -C /opt/
COPY opt/entry/19-wrapper-properties.sh \
     opt/entry/20-gocd-render-autoregister-conf.sh \
     opt/entry/99-chown-gocd-dir.sh \
     /opt/entry/
COPY opt/qnib/gocd/bin/start.sh /opt/qnib/gocd/bin/start.sh
COPY opt/qnib/gocd/etc/autoregister.properties /opt/qnib/gocd/etc/
CMD ["/opt/qnib/gocd/bin/start.sh"]
