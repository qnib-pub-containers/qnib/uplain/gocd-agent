#!/bin/bash

cat /opt/go-agent/wrapper-config/wrapper-properties.conf \
    |sed -e "s#wrapper.app.parameter.101=.*#wrapper.app.parameter.101=${GO_SERVER_URL}#" \
    |sponge /opt/go-agent/wrapper-config/wrapper-properties.conf
